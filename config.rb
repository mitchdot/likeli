###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
  activate :php
end

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

# Use directory indexes
activate :directory_indexes

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

set :partials_dir, 'partials'

set :fonts_dir, 'fonts'

activate :sitemap, :hostname => "http://www.likeli.co"

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end

# Middleman-Deploy script
# activate :deploy do |deploy|
#   deploy.method   = :sftp
#   deploy.host     = "mitchpruitt.com"
#   deploy.port     = 22
#   deploy.path     = "/home/mitchpru/www/likeli"
#   # Optional Settings
#   deploy.user     = "mitchpru"
#   # deploy.password = "secret" # no default
#   deploy.build_before = true
# end

activate :deploy do |deploy|
  deploy.method   = :ftp
  deploy.host     = "likeli.co"
  # deploy.port     = 22
  deploy.path     = "/httpdocs"
  # Optional Settings
  deploy.user     = "LikeliAdmin"
  deploy.password = "R3l4t10nship"
  deploy.build_before = true
end
